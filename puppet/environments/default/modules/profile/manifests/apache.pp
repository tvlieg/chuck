class profile::apache {
  class { 'apache':
    default_vhost => false,
    default_mods  => false,
    mpm_module    => 'prefork',
  }

  include apache::mod::php

  apache::vhost { 'example.com':
    port    => '80',
    docroot => '/var/www',
  }

  file { '/var/www/info.php':
    ensure => present,
    source => 'puppet:///modules/profile/info.php',
  }

  file { '/var/www/index.php':
    ensure => present,
    source => 'puppet:///modules/profile/index.php',
  }
}
