node default {

  package { 'cowsay-morecows':
    ensure   => installed,
    source   => 'http://www.melvilletheatre.com/articles/el7/cowsay-morecows-1.0-1.el7.centos.noarch.rpm',
    provider => 'rpm',
    require  => Package['cowsay'],
  }

  package { 'cowsay':
    ensure   => installed,
    source   => 'http://www.melvilletheatre.com/articles/el7/cowsay-3.03-14.el7.centos.noarch.rpm',
    provider => 'rpm',
  }

  include profile::apache
}
